// Since octet's dynarray and string containers are
// unreliable on copying therefore using STL alternatives
#include <string>
#include <vector>
#include <stack>

// Reference: http://stackoverflow.com/questions/1644868/c-define-macro-for-debug-printing
#ifdef _DEBUG
#define DEBUG_PRINT(fmt, ...) do{ printf(fmt, __VA_ARGS__); } while(0)
#endif
#ifndef _DEBUG
#define DEBUG_PRINT(fmt, ...)
#endif

namespace octet {
  
  // --

  namespace util {
    
    /**
     *  Utility class which manages input.
     *  Used to delay key-press.
     */
    class InputManager {
    private:
      app* _app;

      // The key delay amount per poll request
      unsigned int _keyDelay;

      // Delay accumulator for processing key presses
      unsigned int _delay;

      // Non-copyable entity
      InputManager(const InputManager&);
      InputManager& operator=(const InputManager&);

    public:
      typedef unsigned int keycode;

      explicit InputManager(app* app = NULL, unsigned int keyDelay = 5) :
        _app(app),
        _keyDelay(keyDelay),
        _delay(0) {
      };

      ~InputManager() {
      };

      /**
       *  Attaches this manager instance with the
       *  specified app instance.
       *  @param app The app which requires input handling
       */
      void attach(app* app) {
        _app = app;
        _delay = 0;
      };
      
      /**
       *  Specify the key delay
       *  @param keyDelay The key delay
       */
      void setKeyDelay(unsigned int keyDelay) {
        _keyDelay = keyDelay;
        _delay = 0;
      };
      
      /**
       *  @param key The keycode to check for
       *  @return true if the specified key is pressed; false otherwise
       */
      bool isKeyDown(keycode key) const {
        return _delay == 0 && _app != NULL && _app->is_key_down(key);
      };
      
      /**
       *  @param key The keycode to check for
       *  @return true if the specified key is pressed and handled internally; false otherwise
       */
      bool processKey(keycode key) {
        if (isKeyDown(key)) {
          _delay = _keyDelay;
          return true;
        };

        return false;
      };
      
      /**
       *  Check for new key presses
       */
      void poll() {
        _delay = (_delay == 0 ? 0 : _delay - 1);
      };
    };

    // --
    
    /**
     *  Utility class which handles mouse input and
     *  translates it into camera movement.
     */
    class MouseCameraManager {
    private:
      app* _app;

      // octet's mouse_ball implementation for rotation of objects
      mouse_ball _rotation;
      float _distance;
      float _sensitivity;

      // Mouse Wheel
      int _clicks;
      float _zoomFactor;
      float _maxZoom;
      float _minZoom;

      // Mouse Position
      int _x;
      int _y;
      
      MouseCameraManager(const MouseCameraManager&);
      MouseCameraManager& operator=(const MouseCameraManager&);
      
      /**
       *  Updates the camera to reflect recent mouse movement.
       *  @param cameraToWorld the camera matrix instance to update.
       */
      void updateMovement(mat4t& cameraToWorld) {
        int x = 0;
        int y = 0;
        _app->get_mouse_pos(x, y);

        if ((_x != x || _y != y) && _app->is_key_down(key_lmb)) {
          cameraToWorld.translate(-((float) (x - _x)), (float) (y - _y), 0.f);
        };

        _x = x;
        _y = y;
      };
      
      /**
       *  Updates the camera to reflect rotation.
       *  @param cameraToWorld the camera matrix instance to update.
       */
      void updateRotation(mat4t& cameraToWorld) {
        _rotation.update(cameraToWorld);
      };
      
      /**
       *  Updates the camera to reflect recent mouse wheel zoom differences.
       *  @param cameraToWorld the camera matrix instance to update.
       */
      void updateZooming(mat4t& cameraToWorld) {
        int clicks = _app->get_mouse_wheel();

        if (clicks != _clicks) {
          float distance = ((clicks - _clicks) * _zoomFactor);
          //setDistance(distance);

          cameraToWorld.translate(0.f, 0.f, -distance);
          cameraToWorld[3].z() = std::min(_maxZoom, std::max(_minZoom, cameraToWorld[3].z()));
        }

        _clicks = clicks;
      };

    public:
      explicit MouseCameraManager(app* app = NULL) :
        _app(app),
        _rotation(),
        _distance(0.f),
        _sensitivity(360.f),
        _clicks(0),
        _zoomFactor(2.f),
        _minZoom(7.f),
        _maxZoom(500.f),
        _x(0), _y(0) {
        _rotation.init(_app, _distance, _sensitivity);
      };

      ~MouseCameraManager() {
      };
      
      /**
       *  Attaches this manager instance with the
       *  specified app instance.
       *  @param app The app which requires input handling
       */
      void attach(app* app) {
        _app = app;
        _rotation.init(_app, _distance, _sensitivity);
      };
      
      /**
       *  Specifiy the sensitvity used for rotation.
       *  @param sensitivity rotation sensitivity
       */
      void setSensitivity(float sensitivity) {
        _sensitivity = sensitivity;
        _rotation.init(_app, _distance, _sensitivity);
      };
      
      /**
       * @return the currently specified rotation sensitivity
       */
      float getSensitivity() const {
        return _sensitivity;
      };
      
      /**
       *  Specifiy the distance used for rotation.
       *  @param distance rotation distance
       */
      void setDistance(float distance) {
        _distance = distance;
        _rotation.init(_app, _distance, _sensitivity);
      };
      
      /**
       *  @return the currently specified rotation distance
       */
      float getDistance() const {
        return _distance;
      };
      
      /**
       *  Specifiy the minimum zoom.
       *  @param minZoom the minimum zoom
       */
      void setMinZoom(float minZoom) {
        _minZoom = minZoom;
      };
      
      /**
       *  @return the currently specified minimum zoom
       */
      float getMinZoom() const {
        return _minZoom;
      };

      /**
       *  Specifiy the maximum zoom.
       *  @param maxZoom the maximum zoom
       */
      void setMaxZoom(float maxZoom) {
        _maxZoom = maxZoom;
      };
      
      /**
       *  @return the currently specified maximum zoom
       */
      float getMaxZoom() const {
        return _maxZoom;
      };

      /**
       *  Specifiy the zoom factor used for zoom.
       *  @param zoomFactor the zoom factor
       */
      void setZoomFactor(float zoomFactor) {
        _zoomFactor = zoomFactor;
      };
      
      /**
       *  @return the currently specified zoom factor
       */
      float getZoomFactor() const {
        return _zoomFactor;
      };

      /**
       *  Updates the provided camera instance to reflect
       *  recent changes in mouse movement.
       *  @param cameraToWorld the camera matrix instance to update
       */
      void update(mat4t& cameraToWorld) {
        updateMovement(cameraToWorld);
        updateRotation(cameraToWorld);
        updateZooming(cameraToWorld);
      };
    };

    // --

    /**
     *  Representation of Colour value
     */
    class Colour {
    private:
      vec4 _colour;

    public:
      explicit Colour(unsigned int red, unsigned int green, unsigned int blue) :
        _colour(red / 255.f, green / 255.f, blue / 255.f, 1.f) {
      };

      explicit Colour(float red, float green, float blue) :
        _colour(red, green, blue, 1.f) {
      };

      const vec4& get() const {
        return _colour;
      };

      vec4& get() {
        return _colour;
      };

      static Colour parse(const char* colour) {
        if (::strcmpi(colour, "black") == 0) {
          return black();
        } else if (::strcmpi(colour, "white") == 0) {
          return white();
        } else if (::strcmpi(colour, "red") == 0) {
          return red();
        } else if (::strcmpi(colour, "green") == 0) {
          return green();
        } else if (::strcmpi(colour, "blue") == 0) {
          return blue();
        } else if (::strlen(colour) == 7 && colour[0] == '#') {
          unsigned int value = 0;

          // Reference: app_utils.h
          for (int i = 1; i < 7; ++i) {
            char c = ::toupper(colour[i]);
            value = value * 16 + (( c <= '9' ? c - '0' : c - 'A' + 10 ) & 0x0F);
          }

          return Colour((value & 0xFF0000) >> 16, (value & 0xFF00) >> 8, value & 0xFF);
        }

        return black();
      };

      static const Colour& black() {
        static Colour black(0.f, 0.f, 0.f);
        return black;
      };
      
      static const Colour& red() {
        static Colour red(1.f, 0.f, 0.f);
        return red;
      };
      
      static const Colour& green() {
        static Colour green(0.f, 1.f, 0.f);
        return green;
      };
      
      static const Colour& blue() {
        static Colour blue(0.f, 0.f, 1.f);
        return blue;
      };

      static const Colour& white() {
        static Colour white(1.f, 1.f, 1.f);
        return white;
      };
    };

    // --

    /**
     *  Colour wheel iterator
     */
    class ColourWheel {
    private:
      enum ColourIndex {
        _Start = -1,

        //Black,
        White,
        Red,
        Green,
        Blue,

        _Length
      };

      int _colourIndex;
      Colour _colour;

      void update() {
        switch (_colourIndex) {
        //case Black: _colour = Colour::black(); return;
        case Red: _colour = Colour::red(); return;
        case Green: _colour = Colour::green(); return;
        case Blue: _colour = Colour::blue(); return;

        case White:
        default: _colour = Colour::white(); return;
        };
      };

    public:
      ColourWheel() :
        _colourIndex(White),
        _colour(Colour::white()) {
      };

      void reset() {
        _colourIndex = White;
        _colour = Colour::white();
      };

      const Colour& operator*() const {
        return _colour;
      };
      
      const Colour* operator->() const {
        return &_colour;
      };

      ColourWheel& operator++() {
        _colourIndex = (_colourIndex + 1) % _Length;
        update();

        return *this;
      };

      ColourWheel& operator--() {
        --_colourIndex;
        if (_colourIndex <= _Start) {
          _colourIndex = _Length - 1;
        }

        update();

        return *this;
      };
    };

    // --

    /**
     * Representation of a bounding box via its 
     * top right and bottom left coordinates.
     */
    struct BoundingBox {
      vec3 topRight;
      vec3 bottomLeft;
    };
    
    // --

    // Reference: invaderers_app.h
    class Sprite {
      // where is our sprite (overkill for a 2D game!)
      mat4t modelToWorld;

      // half the width of the sprite
      float halfWidth;

      // half the height of the sprite
      float halfHeight;

      // what texture is on our sprite
      GLuint texture;

    public:
      Sprite(GLuint texture = 0) :
        modelToWorld(1.f),
        texture(texture) {
      };
        
      void setTexture(GLuint textureHandle) {
        texture = textureHandle;
      };
      
      GLuint getTexture() const {
        return texture;
      };

      void setWidth(float width) {
        halfWidth = width * 0.5f;
      };
      
      float getWidth() const {
        return halfWidth * 2;
      };

      void setHeight(float height) {
        halfHeight = height * 0.5f;
      };
      
      float getHeight() const {
        return halfHeight * 2;
      };

      const mat4t& getModelToWorld() const {
        return modelToWorld;
      };
      
      mat4t& getModelToWorld() {
        return modelToWorld;
      };

      void render(texture_shader &shader, const mat4t &cameraToWorld) const {
        // invisible sprite... used for gameplay.
        if (!texture) return;

        // build a projection matrix: model -> world -> camera -> projection
        // the projection space is the cube -1 <= x/w, y/w, z/w <= 1
        mat4t modelToProjection = mat4t::build_projection_matrix(modelToWorld, cameraToWorld);

        // set up opengl to draw textured triangles using sampler 0 (GL_TEXTURE0)
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);

        // use "old skool" rendering
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        shader.render(modelToProjection, 0);

        // this is an array of the positions of the corners of the sprite in 3D
        // a straight "float" here means this array is being generated here at runtime.
        /*float vertices[] = {
          -halfWidth, -halfHeight, 0,
           halfWidth, -halfHeight, 0,
           halfWidth,  halfHeight, 0,
          -halfWidth,  halfHeight, 0,
        };*/
        
        float vertices[] = {
          -halfWidth, 0,              0,
           halfWidth, 0,              0,
           halfWidth, halfHeight * 2.f, 0,
          -halfWidth, halfHeight * 2.f, 0,
        };
        
        // attribute_pos (=0) is position of each corner
        // each corner has 3 floats (x, y, z)
        // there is no gap between the 3 floats and hence the stride is 3*sizeof(float)
        glVertexAttribPointer(attribute_pos, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)vertices );
        glEnableVertexAttribArray(attribute_pos);
    
        // this is an array of the positions of the corners of the texture in 2D
        static const float uvs[] = {
           0,  0,
           1,  0,
           1,  1,
           0,  1,
        };

        // attribute_uv is position in the texture of each corner
        // each corner (vertex) has 2 floats (x, y)
        // there is no gap between the 2 floats and hence the stride is 2*sizeof(float)
        glVertexAttribPointer(attribute_uv, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)uvs );
        glEnableVertexAttribArray(attribute_uv);
    
        // finally, draw the sprite (4 vertices)
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
      };

    };

  };

  // --

  /** 
   *  LSystem Production Rule representation
   */
  class ProductionRule {
  private:
    std::string _lhs;
    std::string _rhs;

    float _chance;

  public:
    ProductionRule() :
      _chance(1.f) {
    };
    
    void setLHS(const std::string& lhs) {
      _lhs = lhs;
    };

    const std::string& getLHS() const {
      return _lhs;
    };
    
    void setRHS(const std::string& rhs) {
      _rhs = rhs;
    };

    const std::string& getRHS() const {
      return _rhs;
    };

    // -- Stochasticity

    float getChance() const {
      return _chance;
    };

    void setChance(float chance) {
      _chance = chance;
    };
  };
  
  // --
  
  /** 
   *  LSystem Parameter representation
   */
  class LSystemParams {
  private:
    unsigned int _iterations;
    float _forward;
    float _angle;

    util::Colour _colour;

    std::string _axiom;
    std::vector<ProductionRule> _rules;

  public:
    LSystemParams() :
      _iterations(0),
      _forward(1.f),
      _angle(45.f),
      _colour(util::Colour::white()) {
    };

    void reset() {
      _iterations = 0;
      _forward = 10.f;
      _angle = 45.f;

      _rules.clear();
    };
    
    const std::vector<ProductionRule>& getRules() const {
      return _rules;
    };

    std::vector<ProductionRule>& getRules() {
      return _rules;
    };
    
    unsigned int getIterations() const {
      return _iterations;
    };
    
    void setIterations(unsigned int iterations) {
      _iterations = iterations;
    };

    float getForward() const {
      return _forward;
    };

    void setForward(float forward) {
      //_forward = std::max(forward, 0.f);
      _forward = forward;
    };

    float getAngle() const {
      return _angle;
    };

    void setAngle(float angleInDegrees) {
      // Clamp angle between 0 and 360 degrees
      //_angle = std::max(0.f, std::min(360.f, angle));
      _angle = angleInDegrees;
    };
    
    const util::Colour& getColour() const {
      return _colour;
    };

    util::Colour& getColour() {
      return _colour;
    };

    void setColour(const util::Colour& colour) {
      _colour = colour;
    };

    const std::string& getAxiom() const {
      return _axiom;
    };

    void setAxiom(const std::string& axiom) {
      _axiom = axiom;
    };
  };
  
  // --
  
  /** 
   *  LSystem XML Configuration parser
   */
  class LSystemXMLConfig {
  private:
    const char* _path;

    // Parse an XML production rule representation
    void parseProductionRuleParam(const TiXmlElement& element, LSystemParams& params) {
      ProductionRule rule;

      const TiXmlElement* child = element.FirstChildElement();
      for (; child != NULL; child = child->NextSiblingElement()) {
        if (strcmp(child->Value(), "lhs") == 0) {
          rule.setLHS(child->GetText());
        } else if (strcmp(child->Value(), "rhs") == 0) {
          rule.setRHS(child->GetText());
        } else if (strcmp(child->Value(), "chance") == 0) {
          rule.setChance((float) ::atof(child->GetText()));
        }
      };

      params.getRules().push_back(rule);
    }
    
    // Parse an XML LSystem parameter representation
    void parseParam(const TiXmlElement& element, LSystemParams& params) {
      if (strcmp(element.Value(), "iterations") == 0) {
        params.setIterations(::atoi(element.GetText()));
      } else if (strcmp(element.Value(), "forward") == 0) {
        params.setForward((float) ::atof(element.GetText()));
      } else if (strcmp(element.Value(), "angle") == 0) {
        params.setAngle((float) ::atof(element.GetText()));
      } else if (strcmp(element.Value(), "colour") == 0) {
        params.setColour(util::Colour::parse(element.GetText()));
      } else if (strcmp(element.Value(), "axiom") == 0) {
        params.setAxiom(element.GetText());
      } else if (strcmp(element.Value(), "production-rules") == 0) {
        const TiXmlElement* child = element.FirstChildElement();

        for (; child != NULL; child = child->NextSiblingElement()) {
          if (strcmp(child->Value(), "rule") == 0) {
            parseProductionRuleParam(*child, params);
          }
        };
      }
    };
    
    // Parse an XML LSystem configuration
    LSystemParams parse(const TiXmlDocument& xml) {
      LSystemParams params;
    
      const TiXmlElement* root = xml.FirstChildElement();
      if (root != NULL && strcmp(root->Value(), "L-system") == 0) {
        const TiXmlElement* child = root->FirstChildElement();

        for (; child != NULL; child = child->NextSiblingElement()) {
          parseParam(*child, params);
        };
      }

      return params;
    };

  public:
    explicit LSystemXMLConfig(const char* path) :
      _path(path) {
    };

    LSystemParams parse() {
      TiXmlDocument doc;
      if (doc.LoadFile(_path)) {
        return parse(doc);
      }

      return LSystemParams();
    };
  };

  // --

  /**
   * LSystem String Parser which allows for parametric LSystem parsing
   **/
  class LSystemStringParser {
  public:
    class Symbol {
    private:
      friend class LSystemStringParser;

      char _letter;

      // NOTE Currently, we do not evaluate parameters as real
      //      numbers as defined in 1.10.1 Parametric OL-systems of ABOP
      std::vector<std::string> _parameters;

      Symbol() {
      };

    public:
      ~Symbol() {
      };

      char getLetter() const {
        return _letter;
      };

      const std::vector<std::string>& getParameters() const {
        return _parameters;
      };
    };

    class SymbolCallback {
    public:
      ~SymbolCallback() {
      };

      virtual void operator()(const Symbol&) = 0;
    };

  private:
    template <typename Iterator>
    static Iterator parseParameters(Iterator begin, Iterator end, Symbol& symbol) {
      size_t bracketCount = 1;
      
      std::string buffer;
      buffer.resize(10);

      while (begin != end && bracketCount > 0) {
        // Skip whitespace
        //if (*begin == ' ') {
        //  continue;
        //}
        
        if (*begin == '(') {
          ++bracketCount;
        } else if (*begin == ')') {
          --bracketCount;
        }

        if (bracketCount > 0) {
          // Arguments are comma seperated
          if (*begin == ',') {
            symbol._parameters.push_back(buffer);
            buffer.clear();
          } else {
            buffer.push_back(*begin);
          }
        }
      }

      // Do not forget to add the final parameter!
      if (!buffer.empty()) {
        symbol._parameters.push_back(buffer);
      }

      return begin;
    };

  public:
    static void parse(const std::string& string, SymbolCallback* callback) {
      parse(string.begin(), string.end(), callback);
    };

    template <typename Iterator>
    static void parse(Iterator begin, Iterator end, SymbolCallback* callback) {
      Symbol currentSymbol;

      while (begin != end) {
        currentSymbol._letter = *begin;

        ++begin;
        if (begin != end && *begin == '(') {
          begin = parseParameters(begin, end, currentSymbol);
        }

        if (callback != NULL) {
          (*callback)(currentSymbol);
        }
      }
    };
  };

  // --
  
  /**
    * An L-System Sybmol Rewriter
    */
  class LSystemSybmolRewriter : public LSystemStringParser::SymbolCallback {
  private:
    const LSystemParams* _params;
    std::string _rewrite;
    //std::string _buffer;
    
    // Selects a rule from a valid set of rules
    static const ProductionRule* selectRule(const std::vector<const ProductionRule*>& rules) {
      const ProductionRule* rule = (rules.empty() ? NULL : rules[0]);

      if (rules.size() > 1) {
        // Calculate the total chance
        float total = 0;
        for (size_t i = 0; i < rules.size(); ++i) {
          total += rules[i]->getChance();
        }

        // Generate a random floating point number within total range
        float random = (((float) ::rand()) / ((float) RAND_MAX)) * total;
        
        // Select and return the appropriate rule
        for (size_t i = 0; i < rules.size() && total > random; ++i) {
          rule = rules[i];
          total -= rule->getChance();
        }
      }

      return rule;
    };
    
    // Retrieves the associated rule with the provided symbol entry if possible
    const ProductionRule* getRule(const LSystemStringParser::Symbol& symbol) const {
      std::vector<const ProductionRule*> validRules;

      // Accumulate all applicable rules
      for (size_t i = 0; i < _params->getRules().size(); ++i) {
        if (!_params->getRules()[i].getLHS().empty() &&
              _params->getRules()[i].getLHS()[0] == symbol.getLetter()) {
          validRules.push_back(&(_params->getRules()[i]));
        }
      }
      
      // Stochastic selection of Production Rule from valid set
      return selectRule(validRules);
    };

  public:
    explicit LSystemSybmolRewriter(const LSystemParams* params, size_t reserve = 0) :
      _params(params) {
      if (reserve > 0) {
        _rewrite.reserve(reserve);
      }
    };

    void reset() {
      _rewrite.clear();
    };

    const std::string& getRewriteString() const {
      return _rewrite;
    };

    std::string& getRewriteString() {
      return _rewrite;
    };

    void operator()(const LSystemStringParser::Symbol& symbol) {
      const ProductionRule* rule = getRule(symbol);

      if (rule != NULL) {
        _rewrite += rule->getRHS();
      } else {
        _rewrite += symbol.getLetter();
      }
    };
  };

  // --

  /**
   *  LSystem
   */
  class LSystem {
  private:
    LSystemParams _params;

  public:
    /**
     *  LSystem Iterator
     */
    class Iterator {
    private:
      const LSystem* _outerType;

      unsigned int _iteration;
      std::string _state;

      LSystemSybmolRewriter _rewriter;
      
      friend class LSystem;

      Iterator(const LSystem* outerType) :
        _outerType(outerType),
        _rewriter(&(outerType->getParameters()), 30) {
          reset();

          // Initialise Iterator to the iteration as specified in the parameters
          operator+=(_outerType->getParameters().getIterations());
      };

      void reset() {
        _iteration = 0;

        _state.clear();
        _state.append(_outerType->getParameters().getAxiom().c_str());
      };

    public:
      bool hasPrevious() const {
        return _iteration > 0;
      };

      bool hasNext() const {
        // LSystems can keep iterating ad-infinitum
        return true;
      };

      int index() const {
        return _iteration;
      };

      const std::string& operator*() const {
        return _state;
      };

      const std::string* operator->() const {
        return &_state;
      };

      // Prefix
      Iterator& operator--() {
        return operator+=(-1);
      };
      
      // Prefix
      Iterator& operator++() {
        _rewriter.reset();
        LSystemStringParser::parse(_state, &_rewriter);

        _state.swap(_rewriter.getRewriteString());
        ++_iteration;

        return *this;
      };

      Iterator& operator+=(int iterations) {
        // Clamp to 0 iterations as minimium
        unsigned int newIteration = (((int) _iteration + iterations < 0) ? 0 : _iteration + iterations);
        
        // In case a previous iteration is required, restart from beginning
        // NOTE for stochastic L-Systems, the previous stochastic decisions will be forgotten
        if (newIteration < _iteration) {
          reset();
        }

        while (_iteration != newIteration) {
          operator++();
        };

        return *this;
      };
      
      Iterator& operator-=(int iterations) {
        return operator+=(-iterations);
      };
    };
    
    LSystem() {
    };

    LSystem(const LSystemParams& params) :
      _params(params) {
    };

    void reset(const LSystemParams& params) {
      // NOTE at this point, any iterators previously created are invalid
      _params = params;
    };

    const LSystemParams& getParameters() const {
      return _params;
    };

    LSystemParams& getParameters() {
      // NOTE at this point, any iterators previously created may be invalid
      return _params;
    };

    Iterator iterator() const {
      return Iterator(this);
    };
  };

  // --
  
  /**
   *  3D Turtle
   */
  class Turtle3D {
  public:
    class State {
    private:
      vec4 _position;
      mat4t _rotation;
      //bool _draw;

      friend class Turtle3D;

      State() :
        _position(0.f, 0.f, 0.f, 1.f),
        _rotation(1.f) {
        //_draw(true) {
      };

      void reset() {
        _position = vec4(0.f, 0.f, 0.f, 1.f);
        _rotation.loadIdentity();
        //_draw = true;
      };

    public:
      ~State() {
      };
    };

  private:
    State _state;

  public:
    Turtle3D() {
    };
    
    explicit Turtle3D(vec3 position) {
      _state._position = vec4(position, 1.f);
    };

/*
    void penUp() {
      _state._draw = false;
    };

    void penDown() {
      _state._draw = true;
    };
*/

    void forward(float distance) {
      _state._position += _state._rotation.lmul(vec4(0.f, distance, 0.f, 0.f));
    };

    // \,/ [Rh]
    void rotateX(float angleInDegrees) {
      _state._rotation.rotateX(angleInDegrees);
    };

    // &,^ [Rl]
    void rotateY(float angleInDegrees) {
      _state._rotation.rotateY(angleInDegrees);
    };

    // +,- [Ru]
    void rotateZ(float angleInDegrees) {
      _state._rotation.rotateZ(angleInDegrees);
    };

    void reset() {
      _state.reset();
    };

    const vec4& getPosition() const {
      return _state._position;
    };

    const mat4t& getRotation() const {
      return _state._rotation;
    };

/*
    bool getPenState() const {
      return _state._draw;
    };
*/

    // Memento Pattern

    const State getState() const {
      return _state;
    };

    void setState(const State& state) {
      _state = state;
    };
  };

  // --

  /**
   * An abstract interface specifying a LSystem Symbol callback which
   * renders results on screen.
   */
  class LSystemRenderer : public LSystemStringParser::SymbolCallback {
  public:
    virtual void init() = 0;
    virtual void reset() = 0;
    
    virtual const util::BoundingBox& getBounds() const = 0;

    virtual void render(const mat4t& cameraToWorld) = 0;
  };

  // --

  /**
    * An L-System Renderer which renders using lines
    */
  class LSystemLineRenderer : public LSystemRenderer {
  private:
    // Parent LSystem structure
    const LSystem* _lsystem;
     
    // Turtle
    Turtle3D _turtle;
    std::stack<Turtle3D::State> _states;
    
    // L-System bounding box
    util::BoundingBox _bounds;
    
    // L-System vertices
    std::vector<vec3> _lines;

    // OpenGL
    color_shader _shader;
    mat4t _modelToWorld;
    
  public:
    LSystemLineRenderer(const LSystem* lsystem) :
      _lsystem(lsystem),
      _modelToWorld(1.f) {
    };
    
    void init() {
      _shader.init();
    };

    void reset() {
      _turtle.reset();
      while (!_states.empty()) {
        _states.pop();
      }

      _bounds.bottomLeft.x() = _bounds.topRight.x() = _turtle.getPosition().x();
      _bounds.bottomLeft.y() = _bounds.topRight.y() = _turtle.getPosition().y();
      _bounds.bottomLeft.z() = _bounds.topRight.z() = _turtle.getPosition().z();

      _lines.clear();
    };
    
    const util::BoundingBox& getBounds() const {
      return _bounds;
    };

    void operator()(const LSystemStringParser::Symbol& symbol) {
      switch (symbol.getLetter()) {
      case 'F': {
        // Record current position
        _lines.push_back(_turtle.getPosition().xyz());

        _turtle.forward(_lsystem->getParameters().getForward());
        /* DEBUG_PRINT("%s\n", turtle.getPosition().toString()); */

        // Record new position
        _lines.push_back(_turtle.getPosition().xyz());
          
        // Update bounds
        _bounds.bottomLeft.x() = std::min(_bounds.bottomLeft.x(), _turtle.getPosition().x());
        _bounds.bottomLeft.y() = std::min(_bounds.bottomLeft.y(), _turtle.getPosition().y());
        _bounds.topRight.x() = std::max(_bounds.topRight.x(), _turtle.getPosition().x());
        _bounds.topRight.y() = std::max(_bounds.topRight.y(), _turtle.getPosition().y());
        break;
      }
      case 'f': {
        _turtle.forward(_lsystem->getParameters().getForward());
        /* DEBUG_PRINT("%s\n", turtle.getPosition().toString()); */
        break;
      }
      case '[': {
        _states.push(_turtle.getState());
        break;
      }
      case ']': {
        _turtle.setState(_states.top());
        _states.pop();
        break;
      }
      case '+': {
        _turtle.rotateZ(_lsystem->getParameters().getAngle());
        break;
      }
      case '-': {
        _turtle.rotateZ(-_lsystem->getParameters().getAngle());
        break;
      }
      case '&': {
        _turtle.rotateY(_lsystem->getParameters().getAngle());
        break;
      }
      case '^': {
        _turtle.rotateY(-_lsystem->getParameters().getAngle());
        break;
      }
      case '\\': {
        _turtle.rotateX(_lsystem->getParameters().getAngle());
        break;
      }
      case '/': {
        _turtle.rotateX(-_lsystem->getParameters().getAngle());
        break;
      }
      case '|': {
        _turtle.rotateZ(180.f);
        break;
      }
      };
    };

    void render(const mat4t& cameraToWorld) {
      
      // allow Z buffer depth testing (closer objects are always drawn in front of far ones)
      glEnable(GL_DEPTH_TEST);

      mat4t modelToProjection = mat4t::build_projection_matrix(_modelToWorld, cameraToWorld);

      // Render Lines
      _shader.render(modelToProjection, _lsystem->getParameters().getColour().get());

      glVertexAttribPointer(attribute_pos, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *) _lines.data());
      glEnableVertexAttribArray(attribute_pos);
    
      glDrawArrays(GL_LINES, 0, _lines.size());

    };
  };

  // --
  
  /**
    * An L-System Renderer which renders using textured images
    */
  class LSystemTreeRenderer : public LSystemRenderer {
  private:
    // Parent L-System structure
    const LSystem* _lsystem;

    // Turtle
    Turtle3D _turtle;
    std::stack<Turtle3D::State> _states;

    // L-System bounding box
    util::BoundingBox _bounds;

    typedef GLuint TextureHandle;

    // OpenGL texture handling
    texture_shader _shader;
    TextureHandle _branchTexture;
    TextureHandle _leafTexture;

    // Utility class which aggregates Sprite information
    struct SpriteInfo {
      GLuint texture;

      vec4 position;
      mat4t rotation;

      float width;
      float height;
      
      SpriteInfo() {
      };

      SpriteInfo(GLuint texture, const vec4& position, const mat4t& rotation, float width, float height) :
        texture(texture),
        position(position),
        rotation(rotation),
        width(width),
        height(height) {
      };

      util::Sprite& set(util::Sprite& sprite) const {
        sprite.setTexture(texture);

        //sprite.getModelToWorld() = rotation;
        //sprite.getModelToWorld().translate(position.x(), position.y(), position.z());
        
        //mat4t translationMatrix(1.f);
        //translationMatrix[3] = position;
        //sprite.getModelToWorld() = rotation * translationMatrix;
        // -- EQUIVALENT --
        sprite.getModelToWorld() = rotation;
        sprite.getModelToWorld()[3] = position;
        
        sprite.setWidth(width);
        sprite.setHeight(height); 

        return sprite;
      };
    };

    // Buffers for sprite information
    std::vector<SpriteInfo> _branches;
    std::vector<SpriteInfo> _leaves;

    void _drawBranch(const vec4& beginPosition, const vec4& endPosition, const mat4t& rotation) {
      _branches.push_back(SpriteInfo(_branchTexture, beginPosition, rotation, 1.0f, _lsystem->getParameters().getForward()));//(endPosition - beginPosition).length()));
      /* DEBUG_PRINT("%s, %s, %s\n", beginPosition.toString(), endPosition.toString(), rotation.toString()); */
    };

    void _drawLeaf(const vec4& position, const mat4t& rotation) {
      _leaves.push_back(SpriteInfo(_leafTexture, position, rotation, 1.f, 4.f));
    };

  public:
    LSystemTreeRenderer(const LSystem* lsystem) :
      _lsystem(lsystem) {
    };
    
    void init() {
      reset();

      _branchTexture = resources::get_texture_handle(GL_RGBA, "assets/lsystems/branch.gif");
      _leafTexture = resources::get_texture_handle(GL_RGBA, "assets/lsystems/leaf.gif");

      _shader.init();
    };
  
    void reset() {
      _turtle.reset();
      while (!_states.empty()) {
        _states.pop();
      }

      _bounds.bottomLeft.x() = _bounds.topRight.x() = _turtle.getPosition().x();
      _bounds.bottomLeft.y() = _bounds.topRight.y() = _turtle.getPosition().y();
      _bounds.bottomLeft.z() = _bounds.topRight.z() = _turtle.getPosition().z();

      _branches.clear();
      _leaves.clear();
    };

    const util::BoundingBox& getBounds() const {
      return _bounds;
    };

    void operator()(const LSystemStringParser::Symbol& symbol) {
      switch (symbol.getLetter()) {
      case 'F': {
        vec4 position = _turtle.getPosition();

        _turtle.forward(_lsystem->getParameters().getForward());
        /* DEBUG_PRINT("%s\n", turtle.getPosition().toString()); */

        _drawBranch(position, _turtle.getPosition(), _turtle.getRotation());

        // Update bounds
        _bounds.bottomLeft.x() = std::min(_bounds.bottomLeft.x(), _turtle.getPosition().x());
        _bounds.bottomLeft.y() = std::min(_bounds.bottomLeft.y(), _turtle.getPosition().y());
        _bounds.topRight.x() = std::max(_bounds.topRight.x(), _turtle.getPosition().x());
        _bounds.topRight.y() = std::max(_bounds.topRight.y(), _turtle.getPosition().y());
        break;
      }
      case 'X': {
        _drawLeaf(_turtle.getPosition(), _turtle.getRotation());
        break;
      }
      case 'f': {
        _turtle.forward(_lsystem->getParameters().getForward());
        /* DEBUG_PRINT("%s\n", turtle.getPosition().toString()); */
        break;
      }
      case '[': {
        _states.push(_turtle.getState());
        break;
      }
      case ']': {
        _turtle.setState(_states.top());
        _states.pop();
        break;
      }
      case '+': {
        _turtle.rotateZ(_lsystem->getParameters().getAngle());
        break;
      }
      case '-': {
        _turtle.rotateZ(-_lsystem->getParameters().getAngle());
        break;
      }
      case '&': {
        _turtle.rotateY(_lsystem->getParameters().getAngle());
        break;
      }
      case '^': {
        _turtle.rotateY(-_lsystem->getParameters().getAngle());
        break;
      }
      case '\\': {
        _turtle.rotateX(_lsystem->getParameters().getAngle());
        break;
      }
      case '/': {
        _turtle.rotateX(-_lsystem->getParameters().getAngle());
        break;
      }
      case '|': {
        _turtle.rotateZ(180.f);
        break;
      }
      };
    };

    void render(const mat4t& cameraToWorld) {
      // Use a single sprite instance
      util::Sprite sprite;
      
      // don't allow Z buffer depth testing (closer objects are always drawn in front of far ones)
      glDisable(GL_DEPTH_TEST);

      // Draw branches
      for (size_t i = 0; i < _branches.size(); ++i) {
        // Apply the appropriate parameters and render accordingly
        _branches[i].set(sprite).render(_shader, cameraToWorld);
      };

      // Draw leaves
      for (size_t i = 0; i < _leaves.size(); ++i) {
        _leaves[i].set(sprite).render(_shader, cameraToWorld);
      };
    };

  };

  // -- 
  
  class lsystems_app : public app {
  private:
    // Input handler
    util::InputManager _input;
    // Mouse camera handler
    util::MouseCameraManager _mouse;

    // Extra file which is bound to F7
    const char* _extraFile;

    // Current L-System
    LSystem _lsystem;
    LSystem::Iterator _iterator;
    
    // LSystem Renderer
    LSystemTreeRenderer* _treeRenderer;
    LSystemLineRenderer  _lineRenderer;
    
    // Currently selected renderer
    LSystemRenderer* _renderer;

    // Camera
    mat4t _cameraToWorld;

    // Colour wheel for changing colours
    util::ColourWheel _colourWheel;

    static const float getForwardDelta() {
      return 0.5f;
    };
    
    static const float getAngleDelta() {
      return 5.f;
    };
    
    static const unsigned int getIterationDelta() {
      return 1;
    };

    // Reset the camera so that the user can see the whole LSystem in the window
    void resetCamera() {
      _cameraToWorld.loadIdentity();

      float width = _renderer->getBounds().topRight.x() - _renderer->getBounds().bottomLeft.x();
      float height = _renderer->getBounds().topRight.y() - _renderer->getBounds().bottomLeft.y();

      // Midpoints
      float xMid = _renderer->getBounds().topRight.x() - (width / 2);
      float yMid = _renderer->getBounds().topRight.y() - (height / 2);

      float maxDistance = std::max(width, height);
      float zoomOutFactor = 1.f + (maxDistance < 15.f ? 0.f : 0.075f * maxDistance);
      
      // Translate camera in order to make the object
      // visible in the centre and zoom out accordingly.
      _cameraToWorld.translate(xMid, yMid, (7.f * zoomOutFactor));

      // Clamp camera
      _cameraToWorld[3].z() = std::min(_mouse.getMaxZoom(), std::max(_mouse.getMinZoom(), _cameraToWorld[3].z()));
    };

    // compute the new set of lines based on the current iteration and the new angles, forwards etc.
    void compute() {
      _renderer->reset();

      DEBUG_PRINT("%s\n", _iterator->c_str());
      LSystemStringParser::parse(*_iterator, _renderer);

      // Update camera to reflect new changes
      resetCamera();
    };

    // Loads an L-System specification from file
    void load(const char* relativeXMLPath) {
      DEBUG_PRINT("Loading: %s\n", relativeXMLPath);

      _lsystem.reset(LSystemXMLConfig(app_utils::get_path(relativeXMLPath)).parse());
      _iterator = _lsystem.iterator();

      _colourWheel.reset();

      compute();
    };
    
    // Handles user input
    void handleInput() {
      _input.poll();
      
      if (_input.processKey(key_space) || is_key_down(key_rmb)) {
        resetCamera();

      // File loading
      } else if (_input.processKey(key_f1)) {
        load("assets/lsystems/a.xml");
      } else if (_input.processKey(key_f2)) {
        load("assets/lsystems/b.xml");
      } else if (_input.processKey(key_f3)) {
        load("assets/lsystems/c.xml");
      } else if (_input.processKey(key_f4)) {
        load("assets/lsystems/d.xml");
      } else if (_input.processKey(key_f5)) {
        load("assets/lsystems/e.xml");
      } else if (_input.processKey(key_f6)) {
        load("assets/lsystems/f.xml");
      } else if (_input.processKey(key_f7) && _extraFile != NULL) {
        load(_extraFile);

#ifdef _DEBUG
      } else if (_input.processKey(key_f12)) {
        load("assets/lsystems/test.xml");
#endif

      // Parameter modification
      } else if (_input.processKey(key_up)) {
        // Increase forward by x
        _lsystem.getParameters().setForward(_lsystem.getParameters().getForward() + getForwardDelta());
        compute();

        DEBUG_PRINT("up\n");
      } else if (_input.processKey(key_down)) {
        // Decrease forward by x
        _lsystem.getParameters().setForward(_lsystem.getParameters().getForward() - getForwardDelta());
        compute();

        DEBUG_PRINT("down\n");
      } else if (_input.processKey(key_right)) {
        // Increase angle by x
        _lsystem.getParameters().setAngle(_lsystem.getParameters().getAngle() + getAngleDelta());
        compute();

        DEBUG_PRINT("right\n");
      } else if (_input.processKey(key_left)) {
        // Decrease angle by x
        _lsystem.getParameters().setAngle(_lsystem.getParameters().getAngle() - getAngleDelta());
        compute();

        DEBUG_PRINT("left\n");
      } else if (_input.processKey(key_page_up)) {
        // Increase iteration by x
        ++_iterator;
        compute();

        DEBUG_PRINT("page up\n");
      } else if (_input.processKey(key_page_down)) {
        // Decrease iteration by x
        --_iterator;
        compute();

        DEBUG_PRINT("page down\n");
      } else if (_input.processKey(key_tab)) {
        if (_renderer == &_lineRenderer) {
          _renderer = _treeRenderer;
        } else {
          _renderer = &_lineRenderer;
        }

        compute();

        DEBUG_PRINT("tab\n");
      } else if (_input.processKey('J')) {
        ++_colourWheel;
        _lsystem.getParameters().setColour(*_colourWheel);

        DEBUG_PRINT("J\n");
      } else if (_input.processKey('K')) {
        --_colourWheel;
        _lsystem.getParameters().setColour(*_colourWheel);
        
        DEBUG_PRINT("K\n");
      }

      _mouse.update(_cameraToWorld);
    };

    // called every frame to move things
    void simulate() {
      handleInput();
    };

    void parseCmdLine(int argc, char *argv[]) {
      int i = 0;

      while (i < argc) {
        // Specify *relative* directory where the 'assets' folder may be found
        if (
            (::strcmp(argv[i], "--directory") == 0 || ::strcmp(argv[i], "-d") == 0) &&
            argc >= i + 1
          ) {
          app_utils::prefix(argv[++i]);
        } else if (
            (::strcmp(argv[i], "--file") == 0 || ::strcmp(argv[i], "-f") == 0) &&
            argc >= i + 1
          ) {
          _extraFile = argv[++i];
        }

        ++i;
      };
    };

  public:

    // this is called when we construct the class
    lsystems_app(int argc, char **argv) :
      app(argc, argv),
      _extraFile(NULL),
      _lsystem(),
      _iterator(_lsystem.iterator()),
      // Tree Renderer is stored on the heap due to it being a large structure
      _treeRenderer(new LSystemTreeRenderer(&_lsystem)),
      _lineRenderer(&_lsystem),
      _renderer(&_lineRenderer),
      _cameraToWorld(1.f) {
        _input.attach(this);
        _input.setKeyDelay(5);

        _mouse.attach(this);

        parseCmdLine(argc, argv);
    };

    // this is called once OpenGL is initialized
    void app_init() {

      // Initialise random number generator
      ::srand((unsigned int) time(NULL));
      
      _treeRenderer->init();
      _lineRenderer.init();
      
      // Load initial file
      load("assets/lsystems/a.xml");

    };

    // this is called to draw the world
    void draw_world(int x, int y, int w, int h) {
      simulate();

      // set a viewport - includes whole window area
      glViewport(x, y, w, h);

      // clear the background to black
      glClearColor(0, 0, 0, 1);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // allow Z buffer depth testing (closer objects are always drawn in front of far ones)
      glEnable(GL_DEPTH_TEST);

      // allow alpha blend (transparency when alpha channel is 0)
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      
      // Draw LSystem
      _renderer->render(_cameraToWorld);
    };
  };
};

#undef DEBUG_PRINT